import 'dart:convert';

Sentences sentencesFromJson(String str) {
  final jsonData = json.decode(str);
  return Sentences.fromMap(jsonData);
}

String sentencesToJson(Sentences data) {
  final dyn = data.toMap();
  return json.encode(dyn);
}

class Sentences {
  int id;
  String polish;
  String italiano;

  Sentences({
    this.id,
    this.polish,
    this.italiano,
  });

  factory Sentences.fromMap(Map<String, dynamic> json) => Sentences(
        id: json["id"],
        polish: json["polish"],
        italiano: json["italiano"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "polish": polish,
        "italiano": italiano,
      };
  @override
  String toString() {
    // TODO: implement toString

    return 'id = $id, polish = $polish, italiano = $italiano';
  }
}
