// flutter build apk --split-per-abi
// flutter build appbundle
// µ
// ctrl + shift + alt + j - zaznacz wszystkie podobne
import 'package:flutter/material.dart';
import 'package:italiano_sentences/screens/addingScreen.dart';
import 'package:italiano_sentences/screens/edit.dart';
import 'package:italiano_sentences/screens/home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.red,
        accentColor: Colors.amber,
        canvasColor: Color.fromRGBO(255, 254, 229, 1),
        fontFamily: 'Raleway',
        textTheme: ThemeData.light().textTheme.copyWith(
              body1: TextStyle(
                color: Color.fromRGBO(20, 51, 51, 1),
                fontSize: 18,
              ),
              body2: TextStyle(
                fontSize: 18,
                color: Color.fromRGBO(20, 51, 51, 1),
              ),
              title: TextStyle(
                fontSize: 18,
                fontFamily: 'RobotoCondensed',
                fontWeight: FontWeight.bold,
              ),
            ),
      ),
      initialRoute: Home.id,
      routes: {
        Home.id: (context) => Home(),
        AddingScreen.id: (context) => AddingScreen(),
        EditScreen.id: (context) => EditScreen(),
      },
    );
  }
}
