import 'dart:async';
import 'package:italiano_sentences/models/sentences.dart';
import 'package:path/path.dart' as path;
import 'package:sqflite/sqflite.dart' as sql;

class DBHelper {
  static Future<sql.Database> database() async {
    final dbPath = await sql.getDatabasesPath();
    return sql.openDatabase(path.join(dbPath, 'sentences.db'),
        onCreate: (db, version) {
      return db.execute('CREATE TABLE sentences(id INTEGER PRIMARY KEY '
          'AUTOINCREMENT, '
          'polish '
          'TEXT, italiano TEXT)');
    }, version: 3);
  }

  static Future<void> insert(Map<String, Object> data) async {
    final db = await DBHelper.database();
    db.insert(
      'sentences',
      data,
      conflictAlgorithm: sql.ConflictAlgorithm.replace,
    );
  }

  static Future<List<Sentences>> getData() async {
    final db = await DBHelper.database();
    final List<Map<String, dynamic>> maps = await db.query('sentences');
    //return db.query(table);
    // List<Sentences> list =
    // query.isNotEmpty ? query.map((c) => Sentences.fromMap(c)).toList() :
    //[];
    print('dlugosc mapy = ${maps.length}');
    if (maps.length == 0) {
      return null;
    } else {
      return List.generate(maps.length, (i) {
        return Sentences(
          id: maps[i]['id'],
          polish: maps[i]['polish'],
          italiano: maps[i]['italiano'],
        );
      });
    }
  }

  static Future<void> deleteSentence(String tekst) async {
    final db = await DBHelper.database();
    await db.delete('sentences', where: "polish= ?", whereArgs: [tekst]);
  }

  static Future<void> updateSentence(Sentences sentences) async {
    final db = await DBHelper.database();
    await db.update('sentences', sentences.toMap(),
        where: "id= ?", whereArgs: [sentences.id]);
  }
}
