import 'package:flutter/material.dart';

const Color kTextFieldBackground = Color.fromRGBO(48, 42, 41, 0.7);
const Color kBackgroundTransparent = Color.fromRGBO(0, 0, 0, 0.2);
const Color kDeleteColor = Color.fromRGBO(255, 0, 0, 0.3);
const Color kEditColor = Color.fromRGBO(255, 0, 0, 0.1);
