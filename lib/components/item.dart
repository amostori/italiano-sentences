import 'package:flutter/material.dart';
import 'package:italiano_sentences/helpers/dbHelper.dart';

class Item extends StatefulWidget {
  final String polish;
  final String italiano;
  final int indeks;

  Item({this.polish, this.indeks, this.italiano});

  @override
  _ItemState createState() => _ItemState();
}

class _ItemState extends State<Item> {
  String tekst = '';

  bool isClicked = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    goTo(context);
  }

  void printDatabase() async {
    print(await DBHelper.getData());
  }

  void deleteItem() async {
    print('indeks przeslany do Item = ${widget.indeks}');

    await DBHelper.deleteSentence(widget.polish);
    await DBHelper.getData();
    setState(() {});
  }

  void goTo(BuildContext context) {
    if (isClicked) {
      setState(() {
        tekst = widget.italiano;
        isClicked = !isClicked;
      });
    } else {
      setState(() {
        tekst = widget.polish;
        isClicked = !isClicked;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => goTo(context),
      child: Card(
        color: Colors.transparent,
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              tekst,
              style: TextStyle(fontSize: 28, color: Colors.white70),
            ),
          ),
        ),
      ),
    );
  }
}
