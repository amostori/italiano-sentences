import 'dart:async';

class Validators {
  final validateEmail = StreamTransformer<String, String>.fromHandlers(
    handleData: (email, sink) {
      if (email.length > 0) {
        sink.add(email);
      } else {
        sink.addError('Dupa');
      }
    },
  );
  final validatePassword = StreamTransformer<String, String>.fromHandlers(
    handleData: (pass, sink) {
      if (pass.length > 0) {
        sink.add(pass);
      } else {
        sink.addError('Dupa');
      }
    },
  );
}
