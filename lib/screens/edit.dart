import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:italiano_sentences/mixins/validationMixin.dart';
import 'package:italiano_sentences/models/sentences.dart';
import 'package:italiano_sentences/screens/home.dart';
import '../helpers/dbHelper.dart';
import '../constance.dart';

class EditScreen extends StatefulWidget {
  static const String id = 'EditScreen';

  final Sentences sentenceToEdit;

  EditScreen({this.sentenceToEdit});

  @override
  _EditScreenState createState() => _EditScreenState();
}

class _EditScreenState extends State<EditScreen> with ValidationMixin {
  var formKey = GlobalKey<FormState>();
  TextEditingController textFieldControlPolish;
  TextEditingController textFieldControlItaliano;
  String polish = '';
  String italiano = '';
  FocusNode nodeOne = FocusNode();
  FocusNode nodeTwo = FocusNode();
  void printDatabase() async {
    print(await DBHelper.getData());
  }

  void updateItem(Sentences sentences) async {
    await DBHelper.updateSentence(sentences);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    textFieldControlPolish =
        TextEditingController(text: widget.sentenceToEdit.polish);
    textFieldControlItaliano =
        TextEditingController(text: widget.sentenceToEdit.italiano);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          child: Image.asset(
            'assets/backedit.png',
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
          ),
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          body: Form(
            key: formKey,
            child: ListView(
              children: <Widget>[
                SizedBox(height: 64),
                getPolish(),
                SizedBox(height: 32),
                getItaliano(),
                SizedBox(height: 32),
                getButton(),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget getPolish() {
    return Card(
      color: kTextFieldBackground,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: TextFormField(
          maxLines: null,
          focusNode: nodeOne,
          style: TextStyle(fontSize: 28, color: Colors.white70),
          autofocus: true,
          textInputAction: TextInputAction.done,
          controller: textFieldControlPolish,
          onSaved: (value) {
            polish = value;
          },
          onFieldSubmitted: (value) {
            print('polish null??? = $polish');

            FocusScope.of(context).requestFocus(nodeTwo);
          },
          validator: fieldValidation,
        ),
      ),
    );
  }

  Widget getItaliano() {
    return Card(
      color: kTextFieldBackground,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: TextFormField(
          maxLines: null,
          focusNode: nodeTwo,
          style: TextStyle(fontSize: 28, color: Colors.white70),
          textInputAction: TextInputAction.done,
          controller: textFieldControlItaliano,
          onSaved: (value) {
            italiano = value;
          },
          onFieldSubmitted: (value) {
            if (formKey.currentState.validate()) {
              formKey.currentState.save();
              var sentence = Sentences(
                  id: widget.sentenceToEdit.id,
                  polish: polish,
                  italiano: italiano);
              updateItem(sentence);
              Navigator.pushReplacementNamed(context, Home.id);
            }
          },
          validator: fieldValidation,
        ),
      ),
    );
  }

  Widget getButton() {
    return Center(
      child: RaisedButton(
        onPressed: () {
          if (formKey.currentState.validate()) {
            formKey.currentState.save();
            var sentence = Sentences(
                id: widget.sentenceToEdit.id,
                polish: polish,
                italiano: italiano);
            updateItem(sentence);
            Navigator.pushReplacementNamed(context, Home.id);
          }
        },
        elevation: 0,
        color: kTextFieldBackground,
        padding: EdgeInsets.symmetric(vertical: 8, horizontal: 64),
        textColor: Colors.white70,
        child: Text(
          'Zapisz',
          style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}
