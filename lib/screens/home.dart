import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../constance.dart';
import '../helpers/dbHelper.dart';
import '../models/sentences.dart';
import '../components/item.dart';
import 'edit.dart';
import 'addingScreen.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class Home extends StatefulWidget {
  static const String id = 'Home';

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  void goToAddingScreen() async {
    Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => AddingScreen()));
  }

  void deleteItem(String item) async {
    await DBHelper.deleteSentence(item);
    setState(() {});
    printDatabase();
  }

  void updateItem(Sentences item) async {
    // var newSentence = Sentences(id: item.id, polish: 'Kurwa', italiano: 'ma
    // ć');
    // await DBHelper.updateSentence(newSentence);
    // setState(() {});
    //printDatabase();
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (context) => EditScreen(
          sentenceToEdit: item,
        ),
      ),
    );
  }

  void printDatabase() async {
    print(await DBHelper.getData());
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          child: Image.asset(
            'assets/background.png',
            fit: BoxFit.cover,
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
          ),
        ),
        Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: kBackgroundTransparent,
        ),
        Scaffold(
          appBar: AppBar(
            title: Text(
              'Italiano Memo',
              style: TextStyle(color: Colors.white70),
            ),
            backgroundColor: Colors.transparent,
          ),
          backgroundColor: Colors.transparent,
          body: Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: FutureBuilder<List<Sentences>>(
              future: DBHelper.getData(),
              builder: (BuildContext context,
                  AsyncSnapshot<List<Sentences>> snapshot) {
                if (snapshot.hasData) {
                  //print('Nie pusto tu ${snapshot.data}');
                  return Padding(
                    padding: const EdgeInsets.only(bottom: 50),
                    child: listView(snapshot.data),
                  );
                } else {
                  return Center(
                    child: GestureDetector(
                      onTap: goToAddingScreen,
                      child: Center(
                        child: Text(
                          '+',
                          style:
                              TextStyle(fontSize: 300, color: Colors.white70),
                        ),
                      ),
                    ),
                  );
                }
              },
            ),
          ),
          floatingActionButton: FloatingActionButton.extended(
            onPressed: () {
              goToAddingScreen();
            },
            elevation: 0.0,
            label: Text(
              'Dodaj',
              style: TextStyle(color: Colors.white70, fontSize: 24),
            ),
            icon: Icon(
              Icons.add,
              color: Colors.white70,
              size: 24,
            ),
            backgroundColor: kBackgroundTransparent,
          ),
        ),
      ],
    );
  }

  Widget listView(List<Sentences> data) {
    return ListView.builder(
      itemBuilder: (BuildContext context, int index) {
        Sentences item = data[index];
        return Slidable(
          actionPane: SlidableDrawerActionPane(),
          secondaryActions: <Widget>[
            IconSlideAction(
              caption: 'Edytuj',
              color: kEditColor,
              icon: Icons.edit,
              onTap: () => updateItem(item),
            ),
            IconSlideAction(
              caption: 'Usuń',
              color: kDeleteColor,
              icon: Icons.delete,
              onTap: () => deleteItem(item.polish),
            ),
          ],
          child: Item(
            polish: item.polish,
            italiano: item.italiano,
            indeks: index,
          ),
        );
      },
      itemCount: data.length,
    );
  }
}
