import 'package:flutter/cupertino.dart';
import 'package:flutter/cupertino.dart' as prefix0;
import 'package:flutter/material.dart';
import 'package:italiano_sentences/mixins/validationMixin.dart';
import 'package:italiano_sentences/models/sentences.dart';
import 'package:italiano_sentences/screens/home.dart';
import '../helpers/dbHelper.dart';
import '../constance.dart';

class AddingScreen extends StatefulWidget {
  static const String id = 'AddingScreen';

  AddingScreen();

  @override
  _AddingScreenState createState() => _AddingScreenState();
}

class _AddingScreenState extends State<AddingScreen> with ValidationMixin {
  final TextEditingController textFieldControlPolish = TextEditingController();
  final TextEditingController textFieldControlItaliano =
      TextEditingController();
  FocusNode nodeOne = FocusNode();
  FocusNode nodeTwo = FocusNode();
  String polish = '';
  String italiano = '';
  var formKey = GlobalKey<FormState>();

  void printDatabase() async {
    print(await DBHelper.getData());
  }

  void addToDatabase(Sentences sentences) async {
    await DBHelper.insert(sentences.toMap());
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          child: Image.asset(
            'assets/background2.png',
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
          ),
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          body: Form(
            key: formKey,
            child: ListView(
              children: <Widget>[
                SizedBox(height: 64),
                Card(
                  color: kTextFieldBackground,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: getPolishText(),
                  ),
                ),
                SizedBox(height: 32),
                Card(
                  color: kTextFieldBackground,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: getItalianText(),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Center(
                  child: getRaisedButton(),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget getPolishText() {
    return TextFormField(
      maxLines: null,
      onFieldSubmitted: (text) {
        FocusScope.of(context).requestFocus(nodeTwo);
      },
      onSaved: (value) {
        polish = value;
      },
      focusNode: nodeOne,
      style: TextStyle(fontSize: 28, color: Colors.white70),
      autofocus: true,
      textInputAction: TextInputAction.done,
      decoration: InputDecoration(
        labelText: 'Polski',
        labelStyle: TextStyle(color: Colors.white70),
      ),
      controller: textFieldControlPolish,
      validator: fieldValidation,
    );
  }

  Widget getItalianText() {
    return TextFormField(
      maxLines: null,
      focusNode: nodeTwo,
      style: TextStyle(fontSize: 28, color: Colors.white70),
      textInputAction: TextInputAction.done,
      decoration: InputDecoration(
        labelText: 'Italiano',
        labelStyle: TextStyle(color: Colors.white70),
      ),
      controller: textFieldControlItaliano,
      onSaved: (value) {
        italiano = value;
      },
      onFieldSubmitted: (text) {
        if (formKey.currentState.validate()) {
          formKey.currentState.save();
          var sentence = Sentences(polish: polish, italiano: italiano);
          addToDatabase(sentence);
          Navigator.pushReplacementNamed(context, Home.id);
        }
      },
      validator: fieldValidation,
    );
  }

  Widget getRaisedButton() {
    return RaisedButton(
      elevation: 0,
      color: kTextFieldBackground,
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 64),
      textColor: Colors.white70,
      onPressed: () {
        if (formKey.currentState.validate()) {
          formKey.currentState.save();
          var sentence = Sentences(polish: polish, italiano: italiano);
          addToDatabase(sentence);
          Navigator.pushReplacementNamed(context, Home.id);
        }
      },
      child: Text(
        'Zapisz',
        style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
      ),
    );
  }
}
